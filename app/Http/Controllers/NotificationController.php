<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\NotificationPhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

use function Psy\debug;

class NotificationController extends Controller
{
    public function list() {
        $idUser = auth()->user()->id;

        return response()->json(Notification::where('user_id', '=', $idUser)->get()->toArray(), 200);
    }

    public function detail(string $id) {
        $obNotification = Notification::find($id);

        if(!$obNotification instanceof Notification) return response()->json(['error' => 'Recurso não encontrado'], 404);

        $obNotification->phones;

        return response()->json($obNotification->toArray(), 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //VALIDA OS ERROS
        $idUser         = auth()->user()->id;
        $obNotification = (new Notification($request->all()));

        $validator = Validator::make($request->all(), $obNotification->rules());

        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $obNotification->user_id = $idUser;
        $obNotification->hash    = md5(uniqid());
        
        //SALVA A NOTIFICAÇÃO
        $obNotification->save();

        (new NotificationPhone)->saveQueue($obNotification->id, $request->input('phones') ?? []);

        //OBTÉM OS ALVOS DA NOTIFICAÇÃO
        $obNotification->phones;

        return response()->json($obNotification->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $obNotification = Notification::find($id);

        if(!$obNotification instanceof Notification) return response()->json(['error' => 'Recurso não encontrado'], 404);


        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($obNotification->rules() as $input => $regra) {
                
                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }
            
            $request->validate($regrasDinamicas);

        } else {
            $request->validate($obNotification->rules());
        }

        $campos = $request->all();

        if(isset($campos['ativo'])) $campos['ativo'] = $campos['ativo'] ? 's' : 'n';

        $obNotification->fill($campos);
        $obNotification->save();

        if($request->has('phones')) {
            NotificationPhone::where('notification_id', '=', $obNotification->id)->delete();

            (new NotificationPhone)->saveQueue($obNotification->id, $request->input('phones'));
        }

        $obNotification->phones;

        return response()->json($obNotification->toArray(), 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        NotificationPhone::where('notification_id', '=', $id)->delete();
        Notification::destroy($id);

        return response('', 204);
    }

    public function send(Request $request, string $hash) {
        $notification = Notification::where('hash', '=', $hash)->first();

        if(!$notification instanceof Notification) return response()->json(['error' => 'Notificação não encontrada'], 404);
        
        $notification->phones;

        $obService = Notification::getServiceClassByType($notification->type);

        $message = $notification->getFinalMessage($request->input('additional'));

        $errors = [];

        foreach ($notification->phones as $phone) {
            try {
                $obService->fill(['number' => $phone->phone, 'content' => $message])->send();
            } catch (\Exception $th) {
                $errors[] = "{$th->getMessage()}\"";
            }
        }

        return response()->json(['msg' => 'Os serviços de comunicação foram requistados com sucesso e em breve os alvos serão notificados.',  'errors' => $errors], 200);
    }
}
