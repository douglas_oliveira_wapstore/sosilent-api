<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'min:3|max:255',
            'email' => 'max:255|email',
            'phone' => 'max:20',
            'password' => 'confirmed',
        ]);

        if($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $obUser = new User($request->all());

        $obUser->save();
        
        return response()->json($obUser->toArray(), 200);
    }
}
