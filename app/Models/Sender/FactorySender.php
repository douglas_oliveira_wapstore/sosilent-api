<?php

namespace App\Models\Sender;

use App\Models\Sender\TwilioSender;
use Illuminate\Database\Eloquent\Model;

class FactorySender extends Model
{
    private const SENDER_MAPPING = [
        'twilio' => TwilioSender::class
    ];

    public static function getActiveMessageSender() : string{
        $envSender = getenv('MESSAGE_SENDER');

        return self::SENDER_MAPPING[$envSender] ?? false;
    }
}
