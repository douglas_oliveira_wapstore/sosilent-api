<?php

namespace App\Models\Sender;

class AbstractSender implements ISender
{
    public string $content;
    public string $number;
    
    public function sendWhatsapp() : bool {
        return false;
    }

    public function sendSMS() : bool {
        return false;
    }

    public function sendCall() : bool {
        return false;
    }

    /**
     * Get the value of content
     */ 
    public function getContent() {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */ 
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of number
     */ 
    public function getNumber() {
        return $this->number;
    }

    /**
     * Set the value of number
     *
     * @return  self
     */ 
    public function setNumber($number) {
        $this->number = $number;

        return $this;
    }

}
