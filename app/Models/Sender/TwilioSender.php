<?php

namespace App\Models\Sender;

use Illuminate\Support\Facades\Storage;
use Twilio\Rest\Client;

class TwilioSender extends AbstractSender
{
    public string $content;
    public string $number;
    public string $sid;
    public string $token;
    public Client $obTwilio;

    private const WHATSAPP_PREFIX = 'whatsapp';

    public function __construct() {
        $this->sid      = getenv('TWILIO_SID');
        $this->token    = getenv('TWILIO_TOKEN');
        $this->obTwilio = new Client($this->sid, $this->token);
    }

    public function sendWhatsapp() : bool {
        $message = $this->obTwilio->messages->create(
            self::WHATSAPP_PREFIX . ":{$this->number}",
            [
              "from" => self::WHATSAPP_PREFIX . ":" . getenv('TWILIO_PHONE_NUMBER'),
              "body" => $this->content,
            ]
        );
    
        return isset($message->sid);
    }

    public function sendSMS() : bool {
        return false;
    }

    public function sendCall() : bool {
        // A Twilio number you own with Voice capabilities
        $twilio_number = '+19802012785';

        $filePath = $this->generateCallFile();

        $this->obTwilio->account->calls->create(
            $this->number,
            $twilio_number,
            array(
                "url" => $filePath
            )
        );

        Storage::disk('local')->delete($filePath);

        return true;
    }

    private function generateCallFile(): string{
        $filename = md5(uniqid()) . '-' . date('Y-m-d_H-i-s') . '-call.xml';
        
        $xmlContent = view('twilio-call', ['message' => $this->content])->render();

        Storage::disk('local')->put('public/calls/' . $filename, $xmlContent);
        
        return getenv('APP_URL') . '/storage/calls/' . $filename;
    }
}
