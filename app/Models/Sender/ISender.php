<?php

namespace App\Models\Sender;

interface ISender
{
    public function sendWhatsapp() : bool;
    public function sendSMS() : bool;
    public function sendCall() : bool;
}
