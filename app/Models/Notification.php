<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'icon',
        'type',
        'app_name',
        'title',
        'description',
        'message',
        'ativo'
    ];

    public function rules() {
        return [
            'type'    => 'in:whatsapp,call,sms',
            'title'   => 'required|max:255',
            'message' => 'required|min:1'
        ];
    }

    /**
     * Get the user's first name.
     */
    protected function ativo(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => (bool) ($value == 's'),
        );
    }

    public const SERVICE_MAPPING = [
        'whatsapp' => \App\Models\WhatsappModel::class,
        'call'     => \App\Models\CallModel::class
    ];

    public static function getServiceClassByType($type) : INotificationService{
        $className = self::SERVICE_MAPPING[$type];

        return new $className;
    }

    public function getFinalMessage($aditionalInformation) {
        $obUser = User::find($this->user_id);

        if(strlen($aditionalInformation)) $aditionalInformation = "*Informações Adicionais:* \"{$aditionalInformation}\"";

        return "Olá! Um usuário identificado como \"{$obUser->name}\" lhe enviou uma " .
            "mensagem de emergência utilizando os serviços da plataforma \"Notificação Segura\". " .
            "*Segue conteúdo da mensagem:* \"{$this->message}\". {$aditionalInformation}";
    }

    /**
     * Get the phone associated with the user.
     */
    public function phones(): HasMany
    {
        return $this->hasMany(NotificationPhone::class);
    }
}
