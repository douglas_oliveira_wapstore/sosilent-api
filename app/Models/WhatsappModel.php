<?php

namespace App\Models;

use App\Models\Sender\AbstractSender;
use App\Models\Sender\FactorySender;
use App\Models\Sender\ISender;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappModel extends Model implements INotificationService
{
    use HasFactory;

    protected $fillable = [
        'number',
        'content'
    ];

    protected AbstractSender $obMessageSender;

    public function __construct() {
      $obMessageSender = FactorySender::getActiveMessageSender();

      $this->obMessageSender = new $obMessageSender();
    }

    public function send() : bool {
      return $this->obMessageSender
                  ->setContent($this->content)
                  ->setNumber($this->number)
                  ->sendWhatsapp();
    }

}
