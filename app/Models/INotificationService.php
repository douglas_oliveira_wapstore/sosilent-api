<?php

namespace App\Models;

interface INotificationService
{
    public function send() : bool;
}
