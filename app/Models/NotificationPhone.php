<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NotificationPhone extends Model
{
    use HasFactory;

    protected $fillable = [
        'notification_id',
        'phone',
        'name'
    ];

    public function notification() : BelongsTo {
        return $this->belongsTo(Notification::class);
    }

    public function saveQueue($idNotification, $phones) {
        //SALVA OS ALVOS DA NOTIFICAÇÃO
        foreach($phones as $phone) {
            $obNotificationPhone = new NotificationPhone();

            $obNotificationPhone->notification_id = $idNotification;
            $obNotificationPhone->phone = $phone['phone'];
            $obNotificationPhone->name = $phone['name'];

            $obNotificationPhone->save();
        }
    }
}
