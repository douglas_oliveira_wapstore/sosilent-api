<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CallController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WhatsappController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('refresh', [AuthController::class, 'refresh'])->name('refresh');
    Route::get('me', [AuthController::class, 'me'])->name('me');

});

Route::post('/user/create', [UserController::class, 'store']);

Route::prefix('notification')->group(function () {
    Route::get('/detail/{id}', [NotificationController::class, 'detail'])->middleware('auth:api');
    Route::get('/list', [NotificationController::class, 'list'])->middleware('auth:api');
    Route::post('/create', [NotificationController::class, 'store'])->middleware('auth:api');
    Route::put('/edit/{id}', [NotificationController::class, 'update'])->middleware('auth:api');
    Route::patch('/edit/{id}', [NotificationController::class, 'update'])->middleware('auth:api');
    Route::delete('/delete/{id}', [NotificationController::class, 'destroy'])->middleware('auth:api');
});

Route::post('/notification/send/{hash}', [NotificationController::class, 'send']);
