<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('notifications', function(Blueprint $table) {
            $table->enum('type', ['whatsapp', 'call', 'sms'])->default('whatsapp')->change();
            $table->text('message')->after('description');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('notifications', function(Blueprint $table) {
            $table->dropColumn(['message']);
        });
    }
};
