<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Este projeto foi desenvolvido como componente de avaliação da disciplina de Aplicativos Mobile do IFSP, e possui como objetivo automatizar, de forma sigilosa, notificações de ajuda para situações de urgência e perigo. Trata-se de uma API em PHP para servir um aplicativo que busca facilitar a criação de denúncias, em que através de interações com o dispositivo (toque acelerado, aumentar/diminuir o volume, pressionar em popup de notificação personalizada e disfarçada) a polícia ou algum parente do cliente pode ser automaticamente acionado via ligação ou mensagem. O usuário do aplicativo pode escolher o tipo da notificação e os gatilhos que a disparam.
As principais tecnologias utilizadas são React Native, no aplicativo, e PHP + Laravel no Backend para envio de notificações utilizando as APIs da Twilio.
